"use strict";

module.exports = function (grunt) {
    var tasks;

    tasks = [
        "grunt-contrib-clean",
        "grunt-contrib-coffee",
        "grunt-contrib-copy",
        "grunt-contrib-stylus",
        "grunt-mocha-test",
        "grunt-umd"
    ];

    grunt.initConfig({
        clean: {
            all: "build",
            source: [
                "build/**/*.coffee",
                "build/**/*.styl"
            ]
        },
        
        coffee: {
            compile: {
                files: [{
                    cwd: "build/lib",
                    dest: "build/lib",
                    expand: true,
                    ext: ".js",
                    src: "*.coffee"
                }],
                options: {
                    bare: true
                }
            }
        },

        copy: {
            bower_components: {
                files: [{
                    cwd: "bower_components",
                    dest: "build/public/lib",
                    expand: true,
                    flatten: true,
                    src: [
                        "backbone/backbone.js",
                        "backbone.babysitter/lib/amd/backbone.babysitter.js",
                        "backbone.marionette/lib/core/amd/backbone.marionette.js",
                        "backbone.wreqr/lib/amd/backbone.wreqr.js",
                        "jquery/dist/jquery.js",
                        "moment/moment.js",
                        "requirejs/require.js",
                        "spin.js/spin.js",
                        "underscore/underscore.js"
                    ]
                }, {
                    cwd: "bower_components/font-awesome",
                    dest: "build/public/lib/font-awesome",
                    expand: true,
                    src: [
                        "css/font-awesome.min.css",
                        "fonts/*"
                    ]
                }, {
                    cwd: "bower_components/select2",
                    dest: "build/public/lib/select2",
                    expand: true,
                    src: [
                        "select2.css",
                        "select2.js",
                        "select2.png",
                        "select2-spinner.gif",
                        "select2x2.png"
                    ]
                }]
            },

            src: {
                files: [{
                    dest: "build/",
                    src: "lib/**/*"
                }, {
                    dest: "build/",
                    src: "public/**/*"
                }]
            }
        },

        mochaTest: {
            test: {
                options: {
                    reporter: "spec",
                    require: "coffee-script"
                },
                src: ["test/**/*.coffee"]
            }
        },

        stylus: {
            compile: {
                files: {
                    "build/public/styles.css": "build/public/styles.styl"
                }
            }
        },

        umd: {
            dateutil: {
                amdModuleId: "dateutil",
                deps: {
                    amd: ["moment-range", "underscore"],
                    cjs: ["moment-range", "underscore"],
                    default: ["moment", "_"]
                },
                dest: "build/lib/dateutil.js",
                globalAlias: "dateutil",
                objectToExport: "dateutil",
                src: "build/lib/dateutil.js"
            },

            scheduler: {
                amdModuleId: "scheduler",
                deps: {
                    amd: ["./dateutil"],
                    cjs: ["./dateutil"],
                    default: ["dateutil"]
                },
                dest: "build/lib/scheduler.js",
                globalAlias: "Scheduler",
                objectToExport: "Scheduler",
                src: "build/lib/scheduler.js"
            },

            select2: {
                amdModuleId: "select2",
                deps: {
                    amd: ["jquery"],
                    cjs: ["jquery"],
                    default: ["jQuery"]
                },
                dest: "build/public/lib/select2/select2.js",
                globalAlias: "Select2",
                objectToExport: "Select2",
                src: "build/public/lib/select2/select2.js"
            },

            room: {
                amdModuleId: "room",
                deps: {
                    amd: ["underscore", "./dateutil", "./rooms", "moment-range", "q"],
                    cjs: ["underscore", "./dateutil", "./rooms", "moment-range", "q"],
                    default: ["_", "dateutil", "rooms", "moment", "Q"]
                },
                dest: "build/lib/room.js",
                globalAlias: "Room",
                objectToExport: "Room",
                src: "build/lib/room.js"
            }
        }
    });

    tasks.forEach(function (task) {
        grunt.loadNpmTasks(task);
    });

    grunt.registerTask("default", [
        "copy:bower_components",
        "copy:src",
        "coffee",
        "umd",
        "stylus",
        "clean:source"
    ]);

    grunt.registerTask("test", [
        "default",
        "mochaTest"
    ]);
};