## Running

1. Start a local instance of [PostgreSQL](http://postgresapp.com/), open up
   `psql`, and create the cache table.

        CREATE TABLE cache (key VARCHAR(255) UNIQUE, value JSON, expires TIMESTAMP WITH TIME ZONE);

2. Build Meeting Mate.

        $ npm install

3. Set the environment variables described in `configuration.js`.

        $ export ADMIN_EMAIL=meeting-mate@atlassian.com
        $ export BASE_URL=http://localhost:8000
        ...

4. Run Meeting Mate.

        $ node build/lib/index.js

5. Navigate to `http://localhost:8000/rooms/refresh` and log in as a user that
   is an administrator of all rooms in the Google Apps domain (you probably want
   to do this in an incognito window).

6. Meeting Mate is now running at `http://localhost:8000`!

## Running Tests

    $ grunt test

## Google Apps integration

You'll need a few things to get Meeting Mate working with Google Apps.

1. **User account** -- needed to get a list of all users in the organisation.
   
    **Admin roles and privileges:**
    
    - Users Read Only

2. **Project** -- needed in order to create API credentials.
3. **Web application (Client ID)** -- needed to allow users to authenticate with Meeting Mate using their Google Apps account.

    A few configuration options are needed:

    - **JavaScript origins:** `http://domain-of-hosted-application` (e.g. http://atlassian-meeting-mate.herokuapp.com/)
    - **Redirect URIs:** `http://domain-of-hosted-application/authenticate/callback` (e.g. http://atlassian-meeting-mate.herokuapp.com/authenticate/callback)

4. **Service account (Client ID)** -- needed to retrieve the list of users, by impersonating the *User account* that was created above.

    A security option is needed: (under *Security → Advanced settings → Manage OAuth Client Access*)

    - `https://www.googleapis.com/auth/admin.directory.user.readonly` API scope -- grant this to the to the Client ID of the service account.

    You might wonder why you need **both** a *User account* **and** a *Service account* with *read users* access. Despite the service account actually having this permission, it still needs to perform the action *as a user*, but it itself can only perform actions that *it too has been granted* — that’s why both are needed.

You'll need to tell Meeting Mate about these things, which can be done via environment variables:

- `GOOGLE_DOMAIN` — the Google Apps domain
- `GOOGLE_PROJECT_ID` — the ID of the project created ⤴︎ in #2
- `GOOGLE_CLIENT_ID` — the Client ID of the Web application created ⤴︎ in #3
- `GOOGLE_CLIENT_SECRET` — the Client ID of the Web application created ⤴︎ in #3
- `SERVICE_ACCOUNT_EMAIL` — the Email address of the Service account created ⤴︎ in #4
- `SERVICE_ACCOUNT_PRIVATE_KEY` — the private key of the Service account created ⤴︎ in #4.

    The private key needs to be in PEM format, e.g.

        Bag Attributes
            friendlyName: privatekey
            localKeyID: …
        Key Attributes: <No Attributes>
        -----BEGIN RSA PRIVATE KEY-----
        …
        -----END RSA PRIVATE KEY-----
        Bag Attributes
            friendlyName: privatekey
            localKeyID: …
        subject=…
        issuer=…
        -----BEGIN CERTIFICATE-----
        …
        -----END CERTIFICATE----- 

    To create this, download the private key in P12 format, then create an unencrypted password-less version using:

        openssl pkcs12 -in service-account.p12 -nodes