var configuration = require("./configuration"),
    GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;

function forwardUser(user, done) {
    done(null, user);
}

function storeAccessToken(accessToken, refreshToken, user, done) {
    user.accessToken = accessToken;
    done(null, user);
}

module.exports = {
    configure: function (passport) {
        // Store user objects in the session as is.
        passport.deserializeUser(forwardUser);
        passport.serializeUser(forwardUser);

        // Use Google OAuth 2 and store the access token on the user.
        passport.use(new GoogleStrategy({
            callbackURL: configuration.BASE_URL + "/authenticate/callback",
            clientID: configuration.GOOGLE_CLIENT_ID,
            clientSecret: configuration.GOOGLE_CLIENT_SECRET,
            scope: ["email", "https://www.googleapis.com/auth/calendar", "profile"]
        }, storeAccessToken));
    }
};
