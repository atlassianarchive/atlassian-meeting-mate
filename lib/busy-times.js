var _ = require("underscore"),
    GoogleCalendar = require("google-calendar"),
    Q = require("q");

/**
 * Get a calendar's batch number.
 *
 * The `freebusy` API only allows querying up to 20 calendars per request. We
 * typically request more than that and thus we must make multiple requests.
 *
 * @param {string} calendar A calendar email address.
 * @param {number} index The calendar's index in the list of all calendars.
 * @returns {number} The index of the request in which this calendar falls.
 */
function getBatchNumber(calendar, index) {
    var maximumCalendars = 20;
    return Math.floor(index / maximumCalendars);
}

/**
 * Get busy information for a set of calendars in a time period.
 *
 * This method cannot handle more than the maximum number of calendars (20).
 *
 * @param {object} options
 * @param {string} options.accessToken The OAuth access token.
 * @param {DateRange} options.range The date range to look in.
 * @param {string[]} calendars The calendars to retrieve.
 * @returns {Q.promise} A promise that is resolved with busy information.
 */
function getBusyTimes(options, calendars) {
    var query;

    query = {
        items: calendars.map(makeItem),
        timeMax: options.range.end.toISOString(),
        timeMin: options.range.start.toISOString()
    };

    return Q
        .ninvoke(GoogleCalendar(options.accessToken).freebusy, "query", query)
        .get("calendars")
        .then(function (calendars) {
            return _.chain(calendars).map(makeBusyPair).object().value();
        });
}

/**
 * @param {object} data Data returned from the `freebusy` API.
 * @param {string} email A calendar email address.
 * @returns {object[]} A calendar email address / busy data pair.
 */
function makeBusyPair(data, email) {
    return [email, data.busy];
}

/**
 * @param {string} calendar A calendar email address.
 * @returns {object} An item suitable to be passed to the `freebusy` API.
 */
function makeItem(calendar) {
    return {
        id: calendar
    };
}

module.exports = {
    /**
     * Get busy information for a set of calendars in a time period.
     *
     * @param {object} options
     * @param {string} options.accessToken The OAuth access token.
     * @param {string[]} options.calendars The calendars to retrieve.
     * @param {DateRange} options.range The date range to look in.
     * @returns {Q.promise} A promise that is resolved with busy information.
     */
    getBusyTimes: function (options) {
        var makePromise = getBusyTimes.bind(this, options),
            promises;

        promises = _
            .chain(options.calendars)
            .groupBy(getBatchNumber)
            .map(makePromise)
            .value();

        return Q.all(promises).then(function (busyTimes) {
            return _.extend.apply(_, busyTimes);
        });
    }
};