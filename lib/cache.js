var _ = require("underscore"),
    cache = {},
    configuration = require("./configuration"),
    moment = require("moment"),
    pg = require("pg"),
    Q = require("q");

/**
 * Execute a database query.
 *
 * @param {string} text The query to execute.
 * @param {...Object} [values] The values to substitute into `text`.
 * @returns {Q.promise} A promise that is resolved with the returned rows.
 */
function query(text, values) {
    values = _.toArray(arguments).slice(1);

    return Q
        .ninvoke(pg, "connect", configuration.DATABASE_URL)
        .spread(function (client, done) {
            return Q
                .ninvoke(client, "query", text, values)
                .then(function (result) {
                    done();
                    return result.rows;
                });
        });
}

/**
 * Remove a value from the cache.
 *
 * @param {string} key The key to remove.
 * @returns {Q.promise} A promise that is resolved on success.
 */
function remove(key) {
    var text;

    delete cache[key];
    text = "DELETE FROM cache WHERE key = $1";
    return query(text, key).thenResolve(undefined);
}

module.exports = {
    /**
     * @param {string} key The key to retrieve.
     * @returns {Q.promise} A promise that is resolved with the value associated
     *     with `key` or rejected.
     */
    get: function (key) {
        var expires,
            hasExpired,
            missMessage = "Couldn't find the key \"" + key + "\".",
            text;

        if (cache[key]) {
            expires = cache[key].expires;
            hasExpired = expires && expires < new Date();

            if (hasExpired) {
                return remove(key).thenReject(missMessage);
            } else {
                return Q(cache[key].value);
            }
        }

        text = "SELECT * FROM cache WHERE key = $1;";
        return query(text, key).then(function (rows) {
            expires = rows[0] && rows[0].expires;
            hasExpired = expires && expires < new Date();

            if (rows.length < 1) {
                throw missMessage;
            } else if (hasExpired) {
                return remove(key).thenReject(missMessage);
            } else {
                cache[key] = {
                    expires: expires,
                    value: rows[0].value
                };

                return rows[0].value;
            }
        });
    },

    /**
     * Put a value into the cache.
     *
     * @param {string} key The key to put.
     * @param {Object} value The value to put.
     * @param {number} [expires] The age at which this value expires (in milliseconds).
     * @returns {Q.promise} A promise that is resolved with `value`.
     */
    put: function (key, value, expires) {
        var expiryDate, expiryString;

        expires = expires && moment().add(expires, "ms");
        expiryDate = expires && expires.toDate();
        expiryString = expires && expires.toISOString();

        return remove(key).then(function () {
            var text = "INSERT INTO cache VALUES ($1, $2, $3);";
            return query(text, key, JSON.stringify(value), expiryString).then(function () {
                cache[key] = {
                    expires: expiryDate,
                    value: value
                };

                return value;
            });
        });
    },

    /**
     * Remove a value from the cache.
     *
     * @param {string} key The key to remove.
     * @returns {Q.promise} A promise that is resolved on success.
     */
    remove: remove
};