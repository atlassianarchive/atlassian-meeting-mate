module.exports = {
    // The email address of an administrative account in the domain. The service
    // account inpersonates this user when retrieving all users in the domain.
    ADMIN_EMAIL: process.env.ADMIN_EMAIL,

    // The base URL without a trailing slash (e.g. http://localhost:8000).
    BASE_URL: process.env.BASE_URL,

    // The URL of the database in which room information is to be stored.
    DATABASE_URL: process.env.DATABASE_URL,

    // The application's OAuth client ID and secret.
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,

    // The Google Apps domain being managed (e.g. atlassian.com).
    GOOGLE_DOMAIN: process.env.GOOGLE_DOMAIN,

    // The application's project ID from the Google Developers Console.
    GOOGLE_PROJECT_ID: process.env.GOOGLE_PROJECT_ID,

    // The Mixpanel <script> tag to include on the page.
    MIXPANEL_SCRIPT: process.env.MIXPANEL_SCRIPT,

    // The OAuth service account's email address and private key.
    SERVICE_ACCOUNT_EMAIL: process.env.SERVICE_ACCOUNT_EMAIL,
    SERVICE_ACCOUNT_PRIVATE_KEY: process.env.SERVICE_ACCOUNT_PRIVATE_KEY,

    // A random string used for session generation.
    SESSION_SECRET: process.env.SESSION_SECRET
};
