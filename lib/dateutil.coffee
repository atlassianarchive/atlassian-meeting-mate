# Turn [[1], [2]] into [1, 2]
flatten = (deep) -> [].concat.apply([], deep)

dateutil =
  # Subtract date ranges from other date ranges.
  # @param {DateRange[]} within The date ranges to look in.
  # @param {DateRange[]} except Busy calendars to exclude.
  # @returns {DateRange[]}
  subtract: (within, except) ->
    except.reduce((frees, busy) ->
      flatten (free.subtract(busy) for free in frees)
    , within)

  # Parse a busy object from the Google Calendar API into a date range.
  # @param {object} busy
  # @returns {DateRange}
  parseBusy: (busy) ->
    moment().range(moment.parseZone(busy.start), moment.parseZone(busy.end))

  # Take something in the form {<email>: [{start: …, end: …}, …], …} and turn it
  # into {<email>: DateRange[], …}
  parseCalendars: (calendars) ->
    _
      .chain(calendars)
      .pairs()
      .map((pair) => [pair[0], _.map(pair[1], @parseBusy)])
      .object()
      .value()

  eachDay: (range, func) ->
    start = moment(range.start).hours(0).minutes(0).seconds(0).milliseconds(0)
    stretched = moment().range(start, range.end)
    stretched.by 'days', func

  # Returns the UTC offset for the location.
  # @param {string} location One of "Sydney", "Vietnam", etc
  # @returns {number} The number of minutes of UTC offset.
  utcOffset: (location) ->
    switch
      when location == "Amsterdam" then -60
      when location == "Gdansk" then -60
      when location == "San Francisco" then 480
      when location == "Sydney" then -660
      when location == "Vietnam" then -240
      else 0

  # @param {string} key Describes the duration, one of "asap", "today", "tomorrow",
  #   "this-week", "next-week"
  # @param {string} location The name of the location: "Sydney", "Vietnam", etc
  # @returns {DateRange}
  duration: (key, location) ->
    zone = @utcOffset(location)
    now = moment().zone(zone)
    switch
      when key == "asap" then moment().range(moment(now), moment(now).endOf('week').add(1, 'hour').endOf('week'))
      when key == "today" then moment().range(moment(now), moment(now).endOf('day'))
      when key == "tomorrow" then moment().range(moment(now).add(1, 'day').startOf('day'), moment(now).add(1, 'day').endOf('day'))
      when key == "this-week" then moment().range(moment(now), moment(now).endOf('week'))
      when key == "next-week" then moment().range(moment(now).endOf('week'), moment(now).endOf('week').add(1, 'hour').endOf('week'))
      else moment().range(moment(now), moment(now).endOf('month'))

      #moment(moment().utc().zone(-660)).endOf('day')