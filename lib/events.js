var GoogleCalendar = require("google-calendar"),
    Q = require("q");

/**
 * @param {string[]} attendees The email addresses of the people/rooms attending.
 * @param {string} calendar The calendar on which the event is to be created.
 * @returns {object[]} An attendees array where the creator has accepted.
 */
function makeAttendees(attendees, calendar) {
    return attendees.map(function (attendee) {
        var isCreator = attendee === calendar;

        return {
            email: attendee,
            responseStatus: isCreator ? "accepted" : "needsAction"
        };
    });
}

module.exports = {
    /**
     * Create an event in Google Calendar.
     *
     * @param {object} options
     * @param {string} options.accessToken An OAuth access token.
     * @param {string[]} options.attendees The email addresses of the people/rooms attending.
     * @param {string} options.calendar The calendar to create the event on.
     * @param {string} options.description The description of the event.
     * @param {moment} options.end When the event ends.
     * @param {string} options.location The location of the event.
     * @param {moment} options.start When the event starts.
     * @param {string} options.title The title of the event.
     * @returns {Q.promise} A promise that is resolved with the event's URL on success.
     */
    createEvent: function (options) {
        var deferred = Q.defer(),
            event;

        event = {
            attendees: makeAttendees(options.attendees, options.calendar),
            description: options.description,
            end: {
                dateTime: options.end.toISOString()
            },
            guestsCanModify: true,
            location: options.location,
            start: {
                dateTime: options.start.toISOString()
            },
            summary: options.title,
            visibility: "public"
        };

        GoogleCalendar(options.accessToken).events.insert(options.calendar, event, {sendNotifications: true}, function (error, response) {
            try {
                deferred.resolve(response.htmlLink);
            } catch (e) {
                deferred.reject(error);
            }
        });

        return deferred.promise;
    }
};
