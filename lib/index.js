var _ = require("underscore"),
    application,
    authentication = require("./authentication"),
    busyTimes = require("./busy-times"),
    configuration = require("./configuration"),
    dateutil = require("./dateutil"),
    events = require("./events"),
    express = require("express"),
    moment = require("moment-range"),
    passport = require("passport"),
    port = Number(process.env.PORT || 8000),
    users = require("./users"),
    Room = require("./room"),
    rooms = require("./rooms"),
    Scheduler = require("./scheduler"),
    Q = require("q");

/**
 * Ensure that the user is authenticated.
 *
 * If not, they are asked to log in and are redirected to the original location.
 */
function ensureAuthenticated(request, response, next) {
    if (request.user) {
        next();
    } else {
        response.redirect("/authenticate?redirect=" + request.url);
    }
}

application = express();
application.configure(function () {
    application.set("view engine", "jade");
    application.set("views", __dirname + "/../public");
    application.use(express.bodyParser());
    application.use(express.cookieParser());
    application.use(express.session({secret: configuration.SESSION_SECRET}));
    application.use(passport.initialize());
    application.use(passport.session());
    application.use("/public", express.static(__dirname + "/../public"));
});

authentication.configure(passport);

/**
 * Meeting Mate's index page.
 */
application.get("/",
    ensureAuthenticated,
    function (request, response) {
        response.render("index", {
            mixpanelScript: configuration.MIXPANEL_SCRIPT,
            user: {
                email: request.user.emails[0].value,
                name: request.user.displayName
            }
        });
    });

/**
 * Start the OAuth dance, passing a redirect target.
 */
application.get("/authenticate",
    function (request) {
        passport.authenticate("google", {
            state: request.query.redirect
        }).apply(passport, arguments);
    });

/**
 * Finish the OAuth dance and redirect to our original location.
 */
application.get("/authenticate/callback",
    passport.authenticate("google"),
    function (request, response) {
        response.redirect(request.query.state);
    });

/**
 * Create an event on the authenticated user's calendar.
 */
application.post("/events",
    function (request, response) {
        var options;

        options = _.extend({}, request.body, {
            accessToken: request.user.accessToken,
            calendar: request.user.emails[0].value,
            end: moment(request.body.end),
            start: moment(request.body.start)
        });

        events.createEvent(options).then(function (URL) {
            response.send(URL);
        });
    });

application.post("/rest/query", function (request, response) {
    var location = request.body.location,
        attendees = request.body.attendees,
        attributes = request.body.attributes,
        duration = moment.duration(request.body.duration, "hours"),
        options,
        range = dateutil.duration(request.body.when, location),
        scheduler = new Scheduler();

    options = {
        accessToken: request.user.accessToken,
        calendars: attendees,
        range: range
    };

    Room.forLocation(location).then(function (rooms) {
        var roomEmails = _.pluck(rooms, "email");

        options.calendars = [].concat(options.calendars).concat(roomEmails);
        busyTimes.getBusyTimes(options).then(
            function (calendars) {
                var except, suggestions;
                calendars = dateutil.parseCalendars(calendars);
                except = _.flatten(_.values(Room.snatchCalendars(rooms, calendars)));
                suggestions = scheduler.suggestions(rooms, range, except);
                suggestions = scheduler.filterAttributes(suggestions, attributes);
                suggestions = scheduler.filterCapacity(suggestions, attendees.length);
                suggestions = scheduler.filterDuration(suggestions, duration);
                suggestions = scheduler.filterWeekends(suggestions);
                response.json(suggestions)
            },
            function (error) {
                response.send(500, JSON.stringify(error));
            }
        );
    });
});

/**
 * Set the OAuth access token used to retrieve rooms.
 */
application.get("/rooms/refresh",
    ensureAuthenticated,
    function (request, response) {
        response.set("Content-Type", "text/plain");
        rooms.setAccessToken(request.user.accessToken)
            .then(rooms.getRooms)
            .then(
                function (rooms) {
                    response.send(rooms.length + " room(s) retrieved.");
                },
                function (error) {
                    response.send(500, "Error: " + error.message);
                }
            );
    });

/**
 * Search for users matching a query.
 */
application.get("/users",
    function (request, response) {
        users.searchUsers(request.query.q).then(function (users) {
            response.json(users);
        });
    });

application.listen(port, function () {
    console.log("Running on port " + port + "...");
});

// Prime the cache.
users.getUsers();
