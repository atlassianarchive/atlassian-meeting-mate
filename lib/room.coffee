class Room
  # @param {string} options.name
  # @param {string} options.email
  # @param {string} options.location
  # @param {number} options.capacity
  # @param {object} options.attributes
  # @param {DateRange[]} options.calendar Busy date ranges.
  constructor: (options) ->
    options = options or {}
    @attributes = options.attributes or {}
    @calendar = options.calendar or []
    @capacity = options.capacity or 0
    @email = options.email
    @location = (options.location or "")
    @name = options.name

  # Calculate the availabilities for a certain date.
  # @returns {DateRange[]} Date ranges of *availability/free*.
  availability: (date) ->
    standard = @standardHours date
    dateutil.subtract standard, @calendar

  standardHours: (date) ->
    date = moment(date).zone(@utcOffset());

    [moment().range(
      moment(date).hours(9),
      moment(date).hours(18)
    )]

  # Returns the UTC offset for the room.
  # @returns {number} The number of minutes offset from UTC.
  utcOffset: ->
    dateutil.utcOffset(@location)

# Retrieve rooms in a location.
# @param {string} location e.g. "Sydney", "San Fransisco"
# @returns {Q} resolved with {Room[]}
Room.forLocation = (location) ->
  rooms.getRooms()
    .then (rooms) ->
      (for room in rooms
        do (room) ->
          if room.location.toLowerCase() == location.toLowerCase()
            new Room({
              attributes: room.attributes,
              name: room.name,
              email: room.email,
              location: room.location,
              capacity: room.capacity
            })
      ).filter((r) -> r != undefined)

# Given some rooms and *busy* calendars, update each room by finding the right
# calendar for the room.
# @param {Room[]} rooms
# @param {{<email>: {DateRange[]}, …}}
Room.snatchCalendars = (rooms, calendars) ->
  calendars = _.clone(calendars)
  for room in rooms
    do (room) ->
      if calendars.hasOwnProperty(room.email)
        room.calendar = calendars[room.email]
        delete calendars[room.email]
        undefined # don't have delete as last line.
  calendars

