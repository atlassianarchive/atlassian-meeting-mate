var _ = require("underscore");

/**
 * @param {object} room An Amsterdam room object.
 * @returns {object|undefined} The parsed version of `room` with attributes, etc.
 */
module.exports = function (room) {
    var components = /AMS - ([\d.]+[A-Z] .*) (\d+)P( VC)?/.exec(room.name);

    if (components) {
        return _.extend(room, {
            attributes: {
                tv: !!components[3],
                vc: !!components[3]
            },
            capacity: Number(components[2]),
            location: "Amsterdam",
            name: components[1]
        });
    }
};