var _ = require("underscore");

/**
 * @param {object} room A Gdansk room object.
 * @returns {object|undefined} The parsed version of `room` with attributes, etc.
 */
module.exports = function (room) {
    var components = /GDN - (.*) (\d+)P( VC)?/.exec(room.name);

    if (components) {
        return _.extend(room, {
            attributes: {
                tv: !!components[3],
                vc: !!components[3]
            },
            capacity: Number(components[2]),
            location: "Gdansk",
            name: components[1]
        });
    }
};