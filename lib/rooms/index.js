var _ = require("underscore"),
    accessToken,
    cache = require("../cache"),
    GoogleCalendar = require("google-calendar"),
    parseAmsterdamRoom = require("./amsterdam"),
    parseGdanskRoom = require("./gdansk"),
    parseSanFranciscoRoom = require("./san-francisco"),
    parseSydneyRoom = require("./sydney"),
    parseVietnamRoom = require("./vietnam"),
    Q = require("q"),
    roomParsers;

/**
 * Cache a list of rooms indefinitely.
 */
function cacheRooms(rooms) {
    return cache.put("rooms", rooms);
}

/**
 * @param {object[]} calendars An array of calendars to filter.
 * @returns {object[]} The subset of given calendars that represent rooms.
 */
function filterRooms(calendars) {
    var resourceId = "@resource.calendar.google.com";
    return calendars.items.filter(function (calendar) {
        return calendar.id.indexOf(resourceId) > -1;
    });
}

/**
 * @param {object} room A room calendar to parse.
 * @returns {object|undefined} A domain object equivalent of `room`.
 */
function parseRoom(room) {
    var location = room.summary.split(" - ")[0],
        parser = roomParsers[location];

    if (parser) {
        return parser({
            email: room.id,
            name: room.summary
        });
    }
}

/**
 * @param {object[]} rooms An array of room calendars to parse.
 * @returns {object[]} An array of parsed room objects.
 */
function parseRooms(rooms) {
    return _.chain(rooms)
            .map(parseRoom)
            .compact()
            .value();
}

/**
 * Retrieve all calendars visible via `accessToken`.
 *
 * @returns {Q.promise} A promise that is resolved with a list of rooms.
 */
function retrieveCalendars() {
    var deferred = Q.defer();

    GoogleCalendar(accessToken).calendarList.list(function (error, response) {
        if (error) {
            deferred.reject(error);
        } else {
            deferred.resolve(response);
        }
    });

    return deferred.promise;
}

roomParsers = {
    AMS: parseAmsterdamRoom,
    GDN: parseGdanskRoom,
    SF: parseSanFranciscoRoom,
    Syd: parseSydneyRoom,
    Vietnam: parseVietnamRoom
};

module.exports = {
    /**
     * Get all rooms in the domain.
     *
     * @return {Q.promise} A promise that is resolved with the list of rooms.
     */
    getRooms: function () {
        return cache.get("rooms").fail(function () {
            return retrieveCalendars()
                .then(filterRooms)
                .then(parseRooms)
                .then(cacheRooms);
        });
    },

    /**
     * Set the Google OAuth access token that is to be used to retrieve rooms.
     *
     * @returns {Q.promise} A promise this is resolved after the access token
     *     has been set.
     */
    setAccessToken: function () {
        accessToken = arguments[0];
        return cache.remove("rooms");
    }
};
