var _ = require("underscore");

/**
 * @param {object} room A San Francisco room object.
 * @returns {object|undefined} The parsed version of `room` with attributes, etc.
 */
module.exports = function (room) {
    var components = /SF - (.*) - (\d+)p( - PB)?( - VC)?/.exec(room.name);

    if (components) {
        return _.extend(room, {
            attributes: {
                phone: !!components[3],
                tv: !!components[4],
                vc: !!components[4]
            },
            capacity: Number(components[2]),
            location: "San Francisco",
            name: components[1]
        });
    }
};