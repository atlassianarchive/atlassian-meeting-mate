var _ = require("underscore");

/**
 * @param {object} room A Sydney room object.
 * @returns {object|undefined} The parsed version of `room` with attributes, etc.
 */
module.exports = function (room) {
    var components = /Syd - ([\d.]+ .*) (\d+)P( VC)?/.exec(room.name);

    if (components) {
        return _.extend(room, {
            attributes: {
                tv: !!components[3],
                vc: !!components[3]
            },
            capacity: Number(components[2]),
            location: "Sydney",
            name: components[1]
        });
    }
};