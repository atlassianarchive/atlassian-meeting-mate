var _ = require("underscore");

/**
 * @param {object} room A Vietnam room object.
 * @returns {object|undefined} The parsed version of `room` with attributes, etc.
 */
module.exports = function (room) {
    var components = /Vietnam - (.*) - (\d+)P( VC)?/.exec(room.name);

    if (components) {
        return _.extend(room, {
            attributes: {
                tv: !!components[3],
                vc: !!components[3]
            },
            capacity: Number(components[2]),
            location: "Vietnam",
            name: components[1]
        });
    }
};