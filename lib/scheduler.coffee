dateutil = require "./dateutil"
moment = require "moment-range"
rooms = require "./rooms"

# Turn [[1], [2]] into [1, 2]
flatten = (deep) -> [].concat.apply([], deep)

serialize = (room, availability) ->
  {
    room: {
      email: room.email
      name: room.name
      location: room.location
      capacity: room.capacity
      attributes: room.attributes
    }
    start: availability.start.toISOString()
    end: availability.end.toISOString()
  }


class Scheduler
  # @param {Room[]} rooms Rooms to check. Their calendars should be populated already.
  # @param {DateRange} during e.g. The period time, e.g. representing "tomorrow" or "next week"
  # @param {DateRange[]} except Date ranges to exclude, e.g. people's "busy" periods
  # @returns {{room: {…}, start: <iso8601>, end: <iso8601>}[]}
  suggestions: (rooms, during, except) ->
    availabilities = []

    dateutil.eachDay during, (date) ->
      for room in rooms
        do (room) ->
          roomIsFree = (during.intersect(a) for a in room.availability(date))
            .filter (a) -> a != null
          for availability in dateutil.subtract(roomIsFree, except)
            do (availability) ->
              availabilities.push serialize(room, availability)

    flatten(availabilities)

  # @param {{room: {...}, start: iso8601, end: iso8601} suggestions
  # @param {{vc: boolean, phone: boolean, tv: boolean}} attributes
  filterAttributes: (suggestions, attributes) ->
    suggestions.filter (suggestion) ->
      for name, required of attributes
        if required and suggestion.room.attributes[name] isnt true
          return false
      true

  filterWeekends: (suggestions) ->
    suggestions.filter (suggestion) ->
      roomZone = dateutil.utcOffset(suggestion.room.location)
      moment.parseZone(suggestion.start).zone(roomZone).isoWeekday() < 6 # 6 = Sat, 7 = Sun

  # @param {{room: {...}, start: iso8601, end: iso8601} suggestions
  # @param {number} minimumCapacity
  filterCapacity: (suggestions, minimumCapacity) ->
    suggestions.filter (suggestion) ->
      suggestion.room.capacity >= minimumCapacity

  # @param {{room: {...}, start: iso8601, end: iso8601} suggestions
  # @param {moment.duration} minimumDuration
  filterDuration: (suggestions, minimumDuration) ->
    suggestions.filter (suggestion) ->
      duration = moment().range(suggestion.start, suggestion.end)
      duration >= minimumDuration
