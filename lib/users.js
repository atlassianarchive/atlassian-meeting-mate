var _ = require("underscore"),
    cache = require("./cache"),
    configuration = require("./configuration"),
    googleapis = require("googleapis"),
    Q = require("q");

/**
 * @param {googleapis/client} client A directory API client.
 * @returns {Q.promise} A promise that is resolved with an authenticated client.
 */
function authenticate(client) {
    var authClient,
        deferred = Q.defer();

    authClient = new googleapis.auth.JWT(
        configuration.SERVICE_ACCOUNT_EMAIL,
        undefined,
        configuration.SERVICE_ACCOUNT_PRIVATE_KEY,
        ["https://www.googleapis.com/auth/admin.directory.user.readonly"],
        configuration.ADMIN_EMAIL
    );

    authClient.authorize(function (error) {
        if (error) {
            deferred.reject(error);
        } else {
            client.withAuthClient(authClient);
            deferred.resolve(client);
        }
    });

    return deferred.promise;
}

/**
 * Cache a list of users for 1 day.
 *
 * @param {object[]} users A list of users.
 * @returns {Q.promise} A promise that is resolved with `users` on success.
 */
function cacheUsers(users) {
    return cache.put("users", users, 1000 * 60 * 60 * 24);
}

/**
 * @returns {Q.promise} A promise that is resolved with a directory API client.
 */
function discoverDirectoryAPI() {
    var deferred = Q.defer();

    googleapis.discover("admin", "directory_v1").execute(function (error, client) {
        if (error) {
            deferred.reject(error);
        } else {
            deferred.resolve(client);
        }
    });

    return deferred.promise;
}

/**
 * @param {object[]} users A list of users as returned by the directory API.
 * @returns {object[]} The parsed version of `users`.
 */
function parseUsers(users) {
    return users.map(function (user) {
        return {
            email: user.primaryEmail,
            familyName: user.name.familyName,
            givenName: user.name.givenName
        };
    });
}

/**
 * Retrieve all users in the domain.
 *
 * The user list API is paginated, this function uses recursion to retrieve all pages.
 *
 * @param {googleapis/client} client A directory API client.
 * @param {string|undefined} [pageToken] A token identifying the page to retrieve.
 * @returns {Q.promise} A promise that is resolved with the list of users.
 */
function retrieveUsers(client, pageToken) {
    var options;

    options = {
        domain: configuration.GOOGLE_DOMAIN,
        maxResults: 500,
        pageToken: pageToken
    };

    return Q
        .ninvoke(client.admin.users.list(options).withAuthClient(client.authClient), "execute")
        .spread(function (response) {
            if (response.nextPageToken) {
                return retrieveUsers(client, response.nextPageToken).then(function (users) {
                    return response.users.concat(users);
                });
            } else {
                return response.users;
            }
        });
}

/**
 * @param {string} query A query string.
 * @param {object} user A user.
 * @returns {boolean} Whether `user` matches `query`.
 */
function userMatchesQuery(query, user) {
    var name = user.givenName + " " + user.familyName;
    return name.toLowerCase().indexOf(query.toLowerCase()) > -1;
}

module.exports = {
    /**
     * Get all users in the domain.
     *
     * The result is cached for 1 day.
     *
     * @returns {Q.promise} A promise that is resolved with the list of users.
     */
    getUsers: function () {
        return cache.get("users").fail(function () {
            return discoverDirectoryAPI()
                .then(authenticate)
                .then(retrieveUsers)
                .then(parseUsers)
                .then(cacheUsers);
        });
    },

    /**
     * Search through all users in the domain.
     *
     * Matches users' first, full, and last names.
     *
     * @param {string} query The query string.
     * @returns {Q.promise} A promise that is resolved with the matching users.
     */
    searchUsers: function (query) {
        return this.getUsers().then(function (users) {
            var matchesQuery = _.partial(userMatchesQuery, query);
            return users.filter(matchesQuery);
        })
    }
};