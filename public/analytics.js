define([
    "jquery"
], function (
    $
) {
    return {
        /**
         * Send an analytics event to Mixpanel.
         *
         * @param {string} name The event name.
         * @param {object} properties The event properties.
         */
        track: function (name, properties) {
            properties = _.extend({}, properties, {
                User: $("html").data("user-email")
            });

            window.mixpanel && mixpanel.track(name, properties);
        }
    };
});