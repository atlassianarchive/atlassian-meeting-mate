define([
    "backbone.marionette"
], function (
    Marionette
) {
    return Marionette.ItemView.extend({
        className: "event-created",

        events: {
            "click .cancel": "onCancel"
        },

        template: _.template([
            "<h1>Scheduled!</h1>",
            "<p>Don't believe me?</p>",
            "<p><a href='<%- URL %>' target='_blank'>See the meeting in Google Calendar.</a></p>",
            "<a class='cancel' href='#'>✕</a>"
        ].join("")),

        onCancel: function (e) {
            e.preventDefault();
            this.close();
        },

        serializeData: function () {
            var userEmail = $("html").data("userEmail");
            return {URL: this.options.URL + "&authuser=" + userEmail};
        }
    });
});
