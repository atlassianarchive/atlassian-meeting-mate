define([
    "jquery",
    "./analytics",
    "backbone.marionette",
    "spin"
], function (
    $,
    Analytics,
    Marionette,
    Spinner
) {
    return Marionette.ItemView.extend({
        className: "event-details",

        events: {
            "click .cancel": "onCancel",
            "click .submit": "onSubmitLink",
            "submit form": "onSubmit"
        },

        template: _.template([
            "<h1>Almost done!</h1>",
            "<p>All we need now is a title and a description.</p>",
            "<form action=''>",
                "<input name='title' placeholder='Give your meeting a useful title' type='text'/>",
                "<textarea name='description' placeholder='Give it a description too if you want'>Booked with Meeting Mate (http://go/meeting).</textarea>",
                "<a class='submit' href='#'></a>",
            "</form>",
            "<a class='cancel' href='#'>✕</a>"
        ].join("")),

        ui: {
            descriptionInput: "textarea",
            form: "form",
            submitButton: ".submit",
            titleInput: "[name=title]"
        },

        /**
         * @returns {string} The event's end time as an ISO 8601 string.
         */
        getEnd: function () {
            var start = moment(this.model.get("start"));
            return start.add(this.options.duration, "hours").toISOString();
        },

        onCancel: function (e) {
            e.preventDefault();
            this.close();
        },

        onShow: function () {
            var attendeeCount = this.options.attendees.length;

            this.ui.titleInput.focus();
            this.ui.submitButton.text([
                "Create meeting and notify",
                attendeeCount,
                attendeeCount == 1 ? "guest" : "guests"
            ].join(" "));
        },

        onSubmit: function (e) {
            var request,
                that = this;

            e.preventDefault();
            this.showLoading();

            request = $.ajax({
                contentType: "application/json",
                data: JSON.stringify({
                    attendees: this.options.attendees.concat(this.model.get("room").email),
                    description: this.ui.descriptionInput.val(),
                    end: this.getEnd(),
                    location: this.model.get("room").name,
                    start: this.model.get("start"),
                    title: this.ui.titleInput.val()
                }),
                type: "POST",
                url: "/events"
            });

            request.done(function (URL) {
                var room = that.model.get("room"),
                    start = moment(that.model.get("start"));

                Analytics.track("Meeting Booked", {
                    "Attendee Count": that.options.attendees.length,
                    "Duration": that.options.duration,
                    "Location": room.location,
                    "Room Capacity": room.capacity,
                    "Room Name": room.name,
                    "Start": start.hour() + start.minute() / 60,
                    "When": that.options.when,
                    "VC": !!that.options.attributes.vc
                });

                that.trigger("create", URL);
            });
        },

        onSubmitLink: function (e) {
            e.preventDefault();
            this.ui.form.submit();
        },

        showLoading: function () {
            this.ui.descriptionInput.attr("disabled", true);
            this.ui.submitButton.text("");
            this.ui.titleInput.attr("disabled", true);

            new Spinner({
                className: "spinner",
                color: "#fff",
                corners: 1,
                direction: 1,
                length: 5,
                lines: 9,
                radius: 4,
                rotate: 0,
                speed: 1,
                trail: 50,
                width: 3
            }).spin(this.ui.submitButton[0]);
        }
    });
});