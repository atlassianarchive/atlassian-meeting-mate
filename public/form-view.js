define([
    "jquery",
    "backbone.marionette",
    "select2"
], function (
    $,
    Marionette
) {
    function loadUsers(query) {
        var request;

        request = $.ajax({
            data: {
                q: query.term
            },
            dataType: "json",
            url: "/users"
        });

        request.done(function (users) {
            users = users.map(function (user) {
                return {
                    id: user.email,
                    text: user.givenName + " " + user.familyName
                }
            });

            query.callback({
                results: users
            });
        });
    }

    return Marionette.ItemView.extend({
        template: _.template([
            "<img class='logo' src='/public/logo.png'/>",
            "<p>Have feedback? Tell us in the Meeting Mate HipChat room!</p>",
            "<form action='' method='post'>",
                "<div class='field'>",
                    "<span class='fa fa-globe'/>",
                    "<select name='location'>",
                        "<option value='Amsterdam'>Amsterdam</option>",
                        "<option value='Gdansk'>Gdansk</option>",
                        "<option value='San Francisco'>San Francisco</option>",
                        "<option selected value='Sydney'>Sydney</option>",
                        "<option value='Vietnam'>Vietnam</option>",
                    "</select>",
                "</div>",
                "<div class='field'>",
                    "<span class='fa fa-group'/>",
                    "<input name='attendees' type='hidden' value='<%- userEmail %>'/>",
                "</div>",
                "<div class='field'>",
                    "<span class='fa fa-calendar-o'/>",
                    "<select name='when'>",
                        "<option value='asap'>Book it ASAP</option>",
                        "<option selected value='today'>Book it today</option>",
                        "<option value='tomorrow'>Book it tomorrow</option>",
                        "<option value='this-week'>Book it this week</option>",
                        "<option value='next-week'>Book it next week</option>",
                    "</select>",
                "</div>",
                "<div class='field'>",
                    "<span class='fa fa-clock-o'/>",
                    "<select name='duration'>",
                        "<option value='0.5'>For half an hour</option>",
                        "<option value='1'>For an hour</option>",
                        "<option value='2'>For 2 hours</option>",
                        "<option value='3'>For 3 hours</option>",
                        "<option value='4'>For 4 hours</option>",
                        "<option value='5'>For 5 hours</option>",
                        "<option value='6'>For 6 hours</option>",
                        "<option value='7'>For 7 hours</option>",
                        "<option value='8'>For 8 hours</option>",
                    "</select>",
                "</div>",
                "<label><input type='checkbox' name='vc'/> Give me VC</label>",
            "</form>"
        ].join("")),

        ui: {
            attendees: "[name=attendees]",
            duration: "[name=duration]",
            location: "[name=location]",
            phone: "[name=phone]",
            tv: "[name=tv]",
            vc: "[name=vc]",
            when: "[name=when]"
        },

        triggers: {
            "change @ui.attendees": "change",
            "change @ui.duration": "change",
            "change @ui.location": "change",
            "change @ui.phone": "change",
            "change @ui.tv": "change",
            "change @ui.vc": "change",
            "change @ui.when": "change"
        },

        getAttendees: function () {
            return this.ui.attendees.select2("val");
        },

        getAttributes: function () {
            return {
                phone: this.ui.phone.is(":checked"),
                tv: this.ui.tv.is(":checked"),
                vc: this.ui.vc.is(":checked")
            };
        },

        getDuration: function () {
            return Number(this.ui.duration.val());
        },

        getLocation: function () {
            return this.ui.location.val();
        },

        getWhen: function () {
            return this.ui.when.val();
        },

        getUserEmail: function () {
            return $("html").data("userEmail");
        },

        getUserName: function () {
            return $("html").data("userName");
        },

        onShow: function () {
            var that = this;

            this.ui.attendees.select2({
                initSelection: function (element, callback) {
                    callback([{
                        id: that.getUserEmail(),
                        text: that.getUserName()
                    }]);
                },
                minimumInputLength: 1,
                multiple: true,
                query: loadUsers
            });
            this.ui.duration.select2();
            this.ui.location.select2();
            this.ui.location.select2("focus");
            this.ui.when.select2();
        },

        reset: function () {
            this.ui.attendees.select2("val", [this.getUserEmail()]);
            this.ui.duration.select2("val", "0.5");
            this.ui.vc.removeAttr("checked");
            this.ui.when.select2("val", "today");
        },

        serializeData: function () {
            return {
                userEmail: $("html").data("userEmail")
            };
        }
    });
});
