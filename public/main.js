requirejs.config({
    baseUrl: "public/lib",
    paths: {
        app: "..",
        select2: "select2/select2"
    },
    shim: {
        backbone: {
            deps: ["jquery", "underscore"],
            exports: "Backbone"
        },
        underscore: {
            exports: "_"
        }
    }
});

require([
    "jquery",
    "app/event-created-view",
    "app/event-details-view",
    "app/form-view",
    "backbone.marionette",
    "app/page-layout",
    "app/result-collection",
    "app/result-collection-view",
    "spin"
], function (
    $,
    EventCreatedView,
    EventDetailsView,
    FormView,
    Marionette,
    PageLayout,
    ResultCollection,
    ResultCollectionView,
    Spinner
) {
    var bodyRegion,
        pageLayout = new PageLayout();

    bodyRegion = new Marionette.Region({
        el: "body"
    });

    bodyRegion.on("show", function () {
        var formView = new FormView(),
            results = new ResultCollection(),
            resultsView;

        function loadResults() {
            var request,
                resultsRegion = pageLayout.resultsRegion.$el,
                spinner;

            resultsRegion.addClass("loading");
            spinner = new Spinner({
                className: "spinner",
                color: "#000",
                length: 20,
                lines: 13,
                radius: 30,
                speed: 1,
                trail: 60,
                width: 10,
                zIndex: 2e9
            }).spin(resultsRegion[0]);

            request = $.ajax({
                contentType: "application/json",
                data: JSON.stringify({
                    attendees: formView.getAttendees(),
                    attributes: formView.getAttributes(),
                    duration: formView.getDuration(),
                    location: formView.getLocation(),
                    when: formView.getWhen()
                }),
                dataType: "json",
                type: "POST",
                url: "/rest/query"
            });

            request.always(function () {
                resultsRegion.removeClass("loading");
                spinner.stop();
            });

            request.done(function (response) {
                results.reset(response);
            });
        }

        formView.on("show", function () {
            // Forgive me.
            _.defer(loadResults);
        });

        resultsView = new ResultCollectionView({
            collection: results
        });

        resultsView.on("itemview:book", function (resultView, result) {
            var eventDetailsView;

            eventDetailsView = new EventDetailsView({
                attendees: formView.getAttendees(),
                attributes: formView.getAttributes(),
                duration: formView.getDuration(),
                model: result,
                when: formView.getWhen()
            });

            eventDetailsView.on("create", function (URL) {
                formView.reset();
                pageLayout.dialogRegion.show(new EventCreatedView({
                    URL: URL
                }));
            });

            pageLayout.dialogRegion.show(eventDetailsView);
        });

        formView.on("change", loadResults);
        pageLayout.formRegion.show(formView);
        pageLayout.resultsRegion.show(resultsView);
    });

    bodyRegion.show(pageLayout);
});
