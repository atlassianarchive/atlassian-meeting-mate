define([
    "underscore",
    "backbone.marionette"
], function (
    _,
    Marionette
) {
    return Marionette.ItemView.extend({
        className: "no-results",
        template: _.template("<p>No results, mate.</p>")
    });
});