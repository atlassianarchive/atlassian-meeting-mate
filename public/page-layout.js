define([
    "underscore",
    "backbone.marionette"
], function(
    _,
    Marionette
) {
    return Marionette.Layout.extend({
        className: "page-layout",

        regions: {
            dialogRegion: ".dialog",
            formRegion: ".form",
            resultsRegion: ".results"
        },

        template: _.template([
            "<div class='form'></div>",
            "<div class='results'></div>",
            "<div class='dialog'></div>",
            "<div class='dialog-blanket'></div>"
        ].join(""))
    });
});