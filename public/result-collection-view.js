define([
    "backbone.marionette",
    "./no-results-view",
    "./result-view"
], function (
    Marionette,
    NoResultsView,
    ResultView
) {
    return Marionette.CollectionView.extend({
        emptyView: NoResultsView,
        itemView: ResultView
    });
});