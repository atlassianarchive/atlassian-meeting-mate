define([
    "backbone",
    "./result-model"
], function (
    Backbone,
    Result
) {
    return Backbone.Collection.extend({
        comparator: function (a, b) {
            var aCapacity = a.get("room").capacity,
                aStart = moment(a.get("start")),
                bCapacity = b.get("room").capacity,
                bStart = moment(b.get("start"));

            // Sort ascending by start time, then ascending by capacity. This
            // encourages people to book rooms that are of an appropriate size.
            return aStart - bStart || aCapacity - bCapacity;
        },

        model: Result
    });
});