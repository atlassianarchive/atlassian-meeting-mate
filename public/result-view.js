define([
    "underscore",
    "backbone.marionette",
    "moment"
], function (
    _,
    Marionette,
    moment
) {
    moment.lang("en", {
        calendar: {
            lastDay: "[Yesterday] LT",
            lastWeek: "[Last] dddd LT",
            nextDay: "[Tomorrow] LT",
            nextWeek: "dddd LT",
            sameDay: "[Today] LT",
            sameElse: "L LT"
        }
    });

    return Marionette.ItemView.extend({
        attributes: function () {
            var data = this.model.toJSON();
            return {
                "data-start": data.start,
                "data-end": data.end
            }
        },

        className: "result",

        events: {
            "click .book": "onBook"
        },

        template: _.template([
            "<h2><%- start %></h2>",
            "<p><%- room.name %> <%- room.capacity %>P</p>",
            "<a class='book' href='#'>Book it!</a>"
        ].join("")),

        onBook: function (e) {
            e.preventDefault();
            this.trigger("book", this.model);
        },

        serializeData: function () {
            var data = this.model.toJSON();

            _.extend(data, {
                start: moment(data.start).calendar()
            });

            return data;
        }
    });
});