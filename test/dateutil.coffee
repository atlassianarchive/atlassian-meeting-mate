should = require "should"
moment = require "moment-range"
dateutil = require "../build/lib/dateutil"

describe "dateutil", ->
  d_1 = new Date 2014, 1, 1
  d_2 = new Date 2014, 2, 2
  d_3 = new Date 2014, 3, 3
  d_4 = new Date 2014, 4, 4
  d_5 = new Date 2014, 5, 5
  d_6 = new Date 2014, 6, 6
  d_7 = new Date 2014, 7, 7

  example =
    start: "2014-02-26T12:00:00+11:00"
    end: "2014-02-26T13:00:00+11:00"

  it "should convert busy objects to moment-range ranges", ->
    expected = moment().range(moment(example.start), moment(example.end))
    dateutil.parseBusy(example).isSame(expected).should.be.true

  describe "#subtract", ->
    within = moment().range d_1, d_7
    free1  = moment().range d_1, d_2
    free2  = moment().range d_3, d_4
    free3  = moment().range d_5, d_7
    busy1  = moment().range d_2, d_3
    busy2  = moment().range d_4, d_5

    it "should allow intersect multiple calendars", ->
      availibilities = dateutil.subtract([within], [busy1, busy2])
      availibilities.should.eql [free1, free2, free3]

  describe "#parseCalendars", ->
    it "should turn start,end into DateRange", ->
      calendars = {
        "foo@bar.com": [
          {start: d_1, end: d_2},
          {start: d_3, end: d_4}
        ]
      }
      parsed = dateutil.parseCalendars(calendars)
      parsed["foo@bar.com"][0].isSame(moment().range(d_1, d_2)).should.be.true

  describe "#duration", ->
    it "should interpret 'asap' as from now, for 2 weeks, honoring the timeZone", ->
      range = dateutil.duration("asap", "San Francisco")
      range.start.zone().should.equal dateutil.utcOffset("San Francisco")
      range.end.zone().should.equal dateutil.utcOffset("San Francisco")
      (moment.duration(7, "days") <= range).should.be.true

    it "should interpret 'today' as from now, to the end of the day, honoring the timeZone", ->
      range = dateutil.duration("today", "San Francisco")
      range.start.zone().should.equal dateutil.utcOffset("San Francisco")
      range.end.should.equal moment().zone(dateutil.utcOffset("San Francisco")).endOf('day')

  describe "#utcOffset", ->
    it "should support 'Sydney'", ->
      dateutil.utcOffset("Sydney").should.eql -660

    it "should support 'Vietnam'", ->
      dateutil.utcOffset("Vietnam").should.eql -240

    it "should support 'San Francisco'", ->
      dateutil.utcOffset("San Francisco").should.eql 480

    it "should default to UTC", ->
      dateutil.utcOffset("Anything else~").should.eql 0