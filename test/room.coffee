dateutil = require "../build/lib/dateutil"
should = require "should"
moment = require "moment-range"
Room = require "../build/lib/room"
Rooms = require "../build/lib/rooms"
sinon = require "sinon"
Q = require "q"

describe "Room", ->

  beforeEach ->
    @vietnamRoom = new Room({capacity: 5, location: "Vietnam", email: "room1@resource.calendar.google.com", name: "Hö"})
    @sydneyRoom  = new Room({capacity: 5, location: "Sydney",  email: "room2@resource.calendar.google.com", name: "Apeture"})
    @sandbox = sinon.sandbox.create()

  afterEach ->
    @sandbox.restore()

  it "should support instantiation", ->
    new Room

  describe "#standardHours", ->
    it "should support Sydney", ->
      room = new Room({
        capacity: 5
        location: "Sydney"
        name: "Kopital"
      })
      date = moment.utc([2014, 4, 1])
      # Sydney is +11:00
      room.standardHours(date)[0].start.isSame(moment.parseZone("2014-05-01T09:00:00.000+11:00")).should.be.true
      room.standardHours(date)[0].end.isSame(moment.parseZone("2014-05-01T18:00:00.000+11:00")).should.be.true

    it "should support Vietnam", ->
      room = new Room({
        capacity: 5
        location: "Vietnam"
        name: "Hö Chï"
      })
      date = moment.utc([2014, 4, 1])

      room.standardHours(date)[0].isSame(moment().range(
        # Vietnam is +04:00
        moment("2014-05-01T09:00:00.000+04:00"),
        moment("2014-05-01T18:00:00.000+04:00")
      )).should.be.true

  describe ".forLocation", ->
    it "should use rooms.getRooms based on location", (done) ->
      rooms = [@vietnamRoom, @sydneyRoom]

      @sandbox.stub Rooms, "getRooms", -> Q.fcall(-> rooms)
      Room.forLocation("Vietnam").done (rooms) =>
        rooms.should.eql [@vietnamRoom]
        done()

  describe "#utcOffset", ->
    it "should call dateutil.utcOffset", ->
      room = new Room(location: "GobdyGook")
      @sandbox.stub dateutil, "utcOffset", (location) ->
        location.should.equal "GobdyGook"
        "wat"
      room.utcOffset().should.equal "wat"
