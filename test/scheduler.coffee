should = require "should"
dateutil = require "../build/lib/dateutil"
sinon = require "sinon"
moment = require "moment-range"
Scheduler = require "../build/lib/scheduler"
Rooms = require "../build/lib/rooms"
Room = require "../build/lib/room"
Q = require "q"

just = (thing) ->
  deferred = Q.defer()
  deferred.resolve(thing)
  deferred.promise

hour = (h) -> new Date(2014, 1, 1, h)

describe "Scheduler", ->

  beforeEach ->
    @sandbox = sinon.sandbox.create()
    @vietnamRoom = new Room {capacity: 5, email: "vietnam@atlassian.com", location: "Vietnam", name: "Hö", attributes: {phone: true}}
    @sydneyRoom  = new Room {capacity: 2, email: "sydney@atlassian.com", location: "Sydney",  name: "Apeture", attributes: {tv: true, vc: true}}

  afterEach ->
    @sandbox.restore()

  it "should allow instantiation", ->
    new Scheduler

  describe "#suggestions", ->

    it "should include partial days when checking room hours", ->
      scheduler = new Scheduler
      @sandbox.spy @vietnamRoom, "availability"
      # this is 3 days — partial, full, partial
      range = moment().range(new Date(2014, 1, 1, 20), new Date(2014, 1, 3, 5))
      scheduler.suggestions([@vietnamRoom], range, [])
      # 1 room, 3 days -> 1 * 3 = 3
      @vietnamRoom.availability.callCount.should.equal 3

    it "should not do overextend days when checking room hours", ->
      scheduler = new Scheduler
      @sandbox.stub @vietnamRoom, "availability", -> []
      # This is one day.
      range = moment().range(new Date(2014, 1, 1), new Date(2014, 1, 1, 23, 59, 59, 999))
      scheduler.suggestions([@vietnamRoom], range, [])
      # 1 room, 1 days -> 1 * 1 = 1
      @vietnamRoom.availability.callCount.should.equal 1

    it "should exclude exceptions", ->
      scheduler = new Scheduler
      @sandbox.stub @vietnamRoom, "availability", ->
        # 4 hour slot from 3am -> 7am
        [moment().range(hour(3), hour(7)),
        # 1 hour slot from 8am -> 9am
         moment().range(hour(8), hour(9))]
      # Look between 12am -> 10am
      within = moment().range(hour(0), hour(10))
      # Exclude 4am -> 6am
      except = [moment().range(hour(4), hour(6))]
      ranges = scheduler.suggestions([@vietnamRoom], within, except)
      # Should have 3am -> 4am, 6am -> 7am, 8am -> 9am
      ranges.should.eql [
        {
          room: {
            email: @vietnamRoom.email
            name: @vietnamRoom.name
            capacity: @vietnamRoom.capacity
            location: @vietnamRoom.location
            attributes: {phone: true}
          }
          start: moment(hour(3)).toISOString()
          end: moment(hour(4)).toISOString()
        },
        {
          room: {
            email: @vietnamRoom.email
            name: @vietnamRoom.name
            capacity: @vietnamRoom.capacity
            location: @vietnamRoom.location
            attributes: {phone: true}
          }
          start: moment(hour(6)).toISOString()
          end: moment(hour(7)).toISOString()
        },
        {
          room: {
            email: @vietnamRoom.email
            name: @vietnamRoom.name
            capacity: @vietnamRoom.capacity
            location: @vietnamRoom.location
            attributes: {phone: true}
          }
          start: moment(hour(8)).toISOString()
          end: moment(hour(9)).toISOString()
        }
      ]

  describe "#filterCapacity", ->
    it "should only return suggestions that have rooms that meet the capacity", ->
      suggestion1 = {
        end: moment().toISOString()
        room: @vietnamRoom
        start: moment().toISOString()
      }
      suggestion2 = {
        end: moment().toISOString()
        room: @sydneyRoom
        start: moment().toISOString()
      }
      scheduler = new Scheduler
      scheduler.filterCapacity([suggestion1, suggestion2], 3).should.eql [suggestion1]

  describe "#filterDuration", ->
    it "should only return suggestions that have a duration equal or greater to the parameter", ->
      suggestion1 = {
        end: moment(hour(5)).toISOString()
        room: @vietnamRoom
        start: moment(hour(4)).toISOString()
      }
      suggestion2 = {
        end: moment(hour(6)).toISOString()
        room: @sydneyRoom
        start: moment(hour(3)).toISOString()
      }
      scheduler = new Scheduler
      scheduler.filterDuration([suggestion1, suggestion2], moment.duration(2, "hours")).should.eql [suggestion2]


  describe "#filterAttributes", ->
    it "should only return suggestions that meet the attributes requirements", ->
      suggestion1 = {
        end: moment(hour(5)).toISOString()
        room: @vietnamRoom
        start: moment(hour(4)).toISOString()
      }
      suggestion2 = {
        end: moment(hour(6)).toISOString()
        room: @sydneyRoom
        start: moment(hour(3)).toISOString()
      }
      scheduler = new Scheduler
      scheduler.filterAttributes([suggestion1, suggestion2], {vc: true, phone: false}).should.eql [suggestion2]